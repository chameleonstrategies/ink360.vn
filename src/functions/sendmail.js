const nodemailer = require('nodemailer');

exports.handler = function (event, context, callback) {

    console.log(event.body);

    const from = "info@mekongmoments.com";

    let transporter = nodemailer.createTransport({
        host: "email-smtp.us-west-2.amazonaws.com",
        port: 465,
        secure: true,
        name: "Mekong Moments",
        email: from,
        auth: {
            user: "AKIAIJHCL54JSXI2F2ZA",
            pass: "AozU84ZZt9gpyuFPaY77xcqJBCFRI8wp8q3WDNdnxfZe"
        }

    });

    const { firstName, lastName, email, phone, message } = JSON.parse(event.body);

    const fullName = (firstName + " " + lastName).trim();

    const recipients = ["reservation.icpq@ihg.com", "aaron.florescabural@ihg.com"];

    const promises = recipients.map(to => {
        return new Promise((resolve, reject) => {
            const mailBody = `
                <table style="width: 100%">
                    <tr>
                        <td style="width: 150px; font-weight: bold;">From</td>
                        <td>${fullName}</td>
                    </tr>
                    <tr>
                        <td style="width: 150px; font-weight: bold;">Email</td>
                        <td>${email}</td>
                    </tr>
                    <tr>
                        <td style="width: 150px; font-weight: bold;">Phone</td>
                        <td>${phone}</td>
                    </tr>
                    <tr>
                        <td style="width: 150px; font-weight: bold;">Message</td>
                        <td>${message}</td>
                    </tr>
                </table>
                `;

                transporter.sendMail({
                    from,
                    to,
                    replyTo: email,
                    subject: `New Message from ${fullName} (${email})`,
                    html: mailBody
                }, function (error, info) {
                    if (error) {
                        reject(error);
                    } else {
                        resolve();
                    }
                });
        });
    });

    Promise.all(promises)
    .then(() => {
        callback(null, {
            statusCode: 200,
            body: "OK"
        })
    })
    .catch(callback);
}